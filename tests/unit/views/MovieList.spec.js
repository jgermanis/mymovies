import { shallowMount } from "@vue/test-utils";
import MovieList from "../../../src/views/MovieList";
import store from "../storeMock";

describe("MovieList.vue cmponent test", () => {
  let cmp;

  beforeEach(() => {
    cmp = shallowMount(MovieList, { store });
  });

  it("has the expected html structure", () => {
    expect(cmp.element).toMatchSnapshot();
  });
});
