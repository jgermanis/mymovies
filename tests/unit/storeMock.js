import Vuex from "vuex";
import Vue from "vue";
import { genresMock, moviesMock } from "./mockData";

Vue.use(Vuex);

const actions = {
  getMovies: jest.fn(),
  initData: jest.fn(),
  getGenres: jest.fn()
};
const getters = {
  filteredMovies(state) {
    return state.movies
      .filter(movie => {
        if (state.showAllGenres) {
          return true;
        } else {
          return movie.genre_ids.every(id => state.selectedGenres.includes(id));
        }
      })
      .filter(movie => {
        return movie.vote_average >= state.minimumRating;
      });
  },
  genres(state) {
    return [
      ...new Set([].concat(...state.movies.map(movie => movie.genre_ids)))
    ];
  },
  getGenreById: state => id => {
    return state.genres.find(genre => genre.id === id);
  },
  loading(state) {
    return state.loading;
  },
  getMinimumRating(state) {
    return state.minimumRating;
  },
  showAllGenres(state) {
    return state.showAllGenres;
  },
  selectedGenres(state) {
    return state.selectedGenres;
  },
  lastLoadedPage(state) {
    return state.lastLoadedPage;
  },
  totalPages(state) {
    return state.totalPages;
  }
};

const mockState = {
  movies: moviesMock,
  genres: genresMock,
  totalPages: 23,
  lastLoadedPage: 1,
  loading: true,
  showAllGenres: false,
  selectedGenres: [12, 28, 53],
  minimumRating: 3
};

const mutations = {
  TOGGLE_GENRE_FILTER: jest.fn(),
  CHANGE_RATING: jest.fn(),
  START_LOADING: jest.fn(),
  STOP_LOADING: jest.fn(),
  SET_MOVIES: jest.fn(),
  SET_GENRES: jest.fn(),
  SELECT_ALL_GENRES: jest.fn(),
  DESELECT_ALL_GENRES: jest.fn()
};
const store = new Vuex.Store({
  state: mockState,
  actions,
  getters,
  mutations
});

export default store;
