import { genresMock, moviesMock } from "../mockData";
import movieList from "../../../src/store/modules/movieList";
import axios from "axios";
const mockState = {
  movies: moviesMock,
  genres: genresMock,
  totalPages: 23,
  lastLoadedPage: 1,
  loading: true,
  showAllGenres: false,
  selectedGenres: [12, 28, 53],
  minimumRating: 3
};

describe("movieList.js", () => {
  describe("getters", () => {
    it("should return correct data on all getters", () => {
      expect(movieList.getters.loading(mockState)).toBe(true);
      expect(movieList.getters.getMinimumRating(mockState)).toBe(3);
      expect(movieList.getters.showAllGenres(mockState)).toBe(false);
      expect(movieList.getters.selectedGenres(mockState)).toEqual([12, 28, 53]);
      expect(movieList.getters.lastLoadedPage(mockState)).toBe(1);
      expect(movieList.getters.totalPages(mockState)).toBe(23);
      expect(movieList.getters.genres(mockState)).toEqual([
        28,
        878,
        53,
        27,
        12,
        18,
        35,
        14
      ]);
      expect(movieList.getters.getGenreById(mockState)(28)).toEqual({
        id: 28,
        name: "Action"
      });
      const filteredMovies = movieList.getters.filteredMovies(mockState);
      expect(filteredMovies.length).toEqual(1);
      expect(filteredMovies[0].id).toEqual(353081);
    });
  });
  describe("mutations", () => {
    beforeEach(() => {
      movieList.state = { ...mockState };
    });
    it("should update store correct on DESELECT_ALL_GENRES", () => {
      movieList.mutations.DESELECT_ALL_GENRES(movieList.state);
      expect(movieList.state.showAllGenres).toEqual(false);
      expect(movieList.state.selectedGenres).toEqual([]);
      expect(movieList.state.genres.some(g => g.active)).toEqual(false);
    });
    it("should update store correct on SELECT_ALL_GENRES", () => {
      movieList.mutations.SELECT_ALL_GENRES(movieList.state);
      expect(movieList.state.showAllGenres).toEqual(true);
      expect(movieList.state.selectedGenres).toEqual([
        28,
        878,
        53,
        27,
        12,
        18,
        35,
        14
      ]);
      expect(movieList.state.genres.some(g => !g.active)).toEqual(false);
    });
    it("should update store correct on SET_GENRES", () => {
      movieList.mutations.SET_GENRES(movieList.state, {
        genres: [{ name: "test", id: 9999999 }]
      });
      expect(movieList.state.genres.length).toEqual(20);
      expect(movieList.state.genres.find(g => g.id === 9999999)).toEqual({
        id: 9999999,
        name: "test"
      });
    });
    it("should update store correct on SET_MOVIES", () => {
      movieList.mutations.SET_MOVIES(movieList.state, {
        results: [{ name: "testMovie", id: 777777 }]
      });
      expect(movieList.state.movies.length).toEqual(7);
      expect(movieList.state.movies.find(m => m.id === 777777)).toEqual({
        id: 777777,
        name: "testMovie"
      });
    });
    it("should update store correct on STOP_LOADING", () => {
      movieList.state.loading = true;
      movieList.mutations.STOP_LOADING(movieList.state);
      expect(movieList.state.loading).toBeFalsy();
    });
    it("should update store correct on START_LOADING", () => {
      movieList.state.loading = false;
      movieList.mutations.START_LOADING(movieList.state);
      expect(movieList.state.loading).toBeTruthy();
    });
    it("should update store correct on CHANGE_RATING", () => {
      movieList.mutations.CHANGE_RATING(movieList.state, 7);
      expect(movieList.state.minimumRating).toEqual(7);
    });
    it("should update store correct on TOGGLE_GENRE_FILTER", () => {
      movieList.mutations.TOGGLE_GENRE_FILTER(movieList.state, 28);
      expect(movieList.state.genres.find(g => g.id === 28).active).toEqual(
        false
      );
      expect(movieList.state.showAllGenres).toBeFalsy();
      expect(movieList.state.selectedGenres.includes(28)).toBeFalsy();
      movieList.mutations.TOGGLE_GENRE_FILTER(movieList.state, 28);
      expect(movieList.state.genres.find(g => g.id === 28).active).toEqual(
        true
      );
      expect(movieList.state.selectedGenres.includes(28)).toBeTruthy();
    });
  });

  describe("actions", () => {
    it("initData should dispatch correct acions and mutations", async () => {
      const testObj = {
        dispatch: () => {
          return Promise.resolve();
        },
        commit: () => {}
      };
      jest.spyOn(testObj, "dispatch");
      jest.spyOn(testObj, "commit");
      await movieList.actions.initData(testObj);
      expect(testObj.dispatch).toHaveBeenCalledWith("getGenres");
      expect(testObj.dispatch).toHaveBeenCalledTimes(2);
      expect(testObj.commit).toHaveBeenCalledWith("SELECT_ALL_GENRES");
    });
    it("getMovies should dispatch correct acions and mutations", async () => {
      const testObj = { commit: () => {} };
      jest.spyOn(testObj, "commit");
      jest.spyOn(axios, "get").mockImplementation(() => {
        return { data: [{ name: "m1", id: 2 }] };
      });
      await movieList.actions.getMovies(testObj);
      expect(axios.get).toHaveBeenCalledWith(
        "https://api.themoviedb.org/3/movie/now_playing?api_key=4e7eee4165fd2f1d26d15b892c9d5f90&language=en-US&page=1"
      );
      expect(testObj.commit.mock.calls).toEqual([
        ["START_LOADING"],
        ["SET_MOVIES", [{ id: 2, name: "m1" }]],
        ["STOP_LOADING"]
      ]);
      jest.spyOn(axios, "get").mockImplementation(() => {
        throw "err";
      });
      jest.spyOn(console, "error").mockImplementation(() => {
        return;
      });
      await movieList.actions.getMovies(testObj);
      expect(console.error).toHaveBeenCalledWith("err");
    });
    it("getGenres should dispatch correct acions and mutations", async () => {
      const testObj = { commit: () => {} };
      jest.spyOn(testObj, "commit");
      axios.get = () => {};
      jest.spyOn(axios, "get").mockImplementation(() => {
        return { data: [{ name: "g1", id: 5 }] };
      });

      await movieList.actions.getGenres(testObj);
      expect(axios.get).toHaveBeenCalledWith(
        "https://api.themoviedb.org/3/genre/movie/list?api_key=4e7eee4165fd2f1d26d15b892c9d5f90&language=en-US"
      );
      expect(testObj.commit.mock.calls).toEqual([
        ["SET_GENRES", [{ id: 5, name: "g1" }]]
      ]);
      jest.spyOn(axios, "get").mockImplementation(() => {
        throw "err";
      });
      jest.spyOn(console, "error").mockImplementation(() => {
        return;
      });
      await movieList.actions.getGenres(testObj);
      expect(console.error).toHaveBeenCalledWith("err");
    });
  });
});
