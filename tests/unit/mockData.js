export const moviesMock = [
  {
    vote_count: 294,
    id: 345940,
    video: false,
    vote_average: 6.3,
    title: "The Meg",
    popularity: 189.501,
    poster_path: "/xqECHNvzbDL5I3iiOVUkVPJMSbc.jpg",
    original_language: "en",
    original_title: "The Meg",
    genre_ids: [28, 878, 53, 27],
    backdrop_path: "/ibKeXahq4JD63z6uWQphqoJLvNw.jpg",
    adult: false,
    overview:
      "A deep sea submersible pilot revisits his past fears in the Mariana Trench, and accidentally unleashes the seventy foot ancestor of the Great White Shark believed to be extinct.",
    release_date: "2018-08-09"
  },
  {
    vote_count: 930,
    id: 353081,
    video: false,
    vote_average: 7.5,
    title: "Mission: Impossible - Fallout",
    popularity: 155.957,
    poster_path: "/AkJQpZp9WoNdj7pLYSj1L0RcMMN.jpg",
    original_language: "en",
    original_title: "Mission: Impossible - Fallout",
    genre_ids: [12, 28, 53],
    backdrop_path: "/5qxePyMYDisLe8rJiBYX8HKEyv2.jpg",
    adult: false,
    overview:
      "When an IMF mission ends badly, the world is faced with dire consequences. As Ethan Hunt takes it upon himself to fulfil his original briefing, the CIA begin to question his loyalty and his motives. The IMF team find themselves in a race against time, hunted by assassins while trying to prevent a global catastrophe.",
    release_date: "2018-07-25"
  },
  {
    vote_count: 408,
    id: 447200,
    video: false,
    vote_average: 6,
    title: "Skyscraper",
    popularity: 91.633,
    poster_path: "/5LYSsOPzuP13201qSzMjNxi8FxN.jpg",
    original_language: "en",
    original_title: "Skyscraper",
    genre_ids: [28, 53, 18],
    backdrop_path: "/oMKFQmoVgB69fyXfSMu0lGlHJP2.jpg",
    adult: false,
    overview:
      "Framed and on the run, a former FBI agent must save his family from a blazing fire in the world's tallest building.",
    release_date: "2018-07-11"
  },
  {
    vote_count: 281,
    id: 455980,
    video: false,
    vote_average: 7,
    title: "Tag",
    popularity: 81.833,
    poster_path: "/eXXpuW2xaq5Aen9N5prFlARVIvr.jpg",
    original_language: "en",
    original_title: "Tag",
    genre_ids: [35, 18],
    backdrop_path: "/yRXzrwLfB5tDTIA3lSU9S3N9RUK.jpg",
    adult: false,
    overview:
      "For one month every year, five highly competitive friends hit the ground running in a no-holds-barred game of tag they’ve been playing since the first grade. This year, the game coincides with the wedding of their only undefeated player, which should finally make him an easy target. But he knows they’re coming...and he’s ready.",
    release_date: "2018-06-14"
  },
  {
    vote_count: 2693,
    id: 351286,
    video: false,
    vote_average: 6.6,
    title: "Jurassic World: Fallen Kingdom",
    popularity: 80.384,
    poster_path: "/c9XxwwhPHdaImA2f1WEfEsbhaFB.jpg",
    original_language: "en",
    original_title: "Jurassic World: Fallen Kingdom",
    genre_ids: [28, 12, 878],
    backdrop_path: "/3s9O5af2xWKWR5JzP2iJZpZeQQg.jpg",
    adult: false,
    overview:
      "Several years after the demise of Jurassic World, a volcanic eruption threatens the remaining dinosaurs on the island of Isla Nublar. Claire Dearing, the former park manager and founder of the Dinosaur Protection Group, recruits Owen Grady to help prevent the extinction of the dinosaurs once again.",
    release_date: "2018-06-06"
  },
  {
    vote_count: 1443,
    id: 363088,
    video: false,
    vote_average: 7,
    title: "Ant-Man and the Wasp",
    popularity: 74.983,
    poster_path: "/rv1AWImgx386ULjcf62VYaW8zSt.jpg",
    original_language: "en",
    original_title: "Ant-Man and the Wasp",
    genre_ids: [28, 12, 14, 35, 878],
    backdrop_path: "/6P3c80EOm7BodndGBUAJHHsHKrp.jpg",
    adult: false,
    overview:
      "As Scott Lang awaits expiration of his term of house detention, Hope van Dyne and Dr. Hank Pym involve him in a scheme to rescue Mrs. van Dyne from the micro-universe into which she has fallen, while two groups of schemers converge on them with intentions of stealing Dr. Pym's inventions.",
    release_date: "2018-07-04"
  }
];

export const genresMock = [
  {
    id: 28,
    name: "Action"
  },
  {
    id: 12,
    name: "Adventure"
  },
  {
    id: 16,
    name: "Animation"
  },
  {
    id: 35,
    name: "Comedy"
  },
  {
    id: 80,
    name: "Crime"
  },
  {
    id: 99,
    name: "Documentary"
  },
  {
    id: 18,
    name: "Drama"
  },
  {
    id: 10751,
    name: "Family"
  },
  {
    id: 14,
    name: "Fantasy"
  },
  {
    id: 36,
    name: "History"
  },
  {
    id: 27,
    name: "Horror"
  },
  {
    id: 10402,
    name: "Music"
  },
  {
    id: 9648,
    name: "Mystery"
  },
  {
    id: 10749,
    name: "Romance"
  },
  {
    id: 878,
    name: "Science Fiction"
  },
  {
    id: 10770,
    name: "TV Movie"
  },
  {
    id: 53,
    name: "Thriller"
  },
  {
    id: 10752,
    name: "War"
  },
  {
    id: 37,
    name: "Western"
  }
];
