import { Constants } from "../../constants";
import axios from "axios";

const state = {
  movies: [],
  genres: [],
  totalPages: null,
  lastLoadedPage: null,
  loading: true,
  showAllGenres: true,
  selectedGenres: [],
  minimumRating: 3
};

const mutations = {
  TOGGLE_GENRE_FILTER(state, genreId) {
    state.showAllGenres = false;
    const index = state.selectedGenres.indexOf(genreId);
    const genre = state.genres.find(genre => genre.id === genreId);
    if (index !== -1) {
      state.selectedGenres.splice(index, 1);
      genre.active = false;
    } else {
      state.selectedGenres.push(genreId);
      genre.active = true;
    }
  },
  CHANGE_RATING(state, newRating) {
    state.minimumRating = newRating;
  },
  START_LOADING(state) {
    state.loading = true;
  },
  STOP_LOADING(state) {
    state.loading = false;
  },
  SET_MOVIES(state, data) {
    state.lastLoadedPage = data.page;
    state.totalPages = data.total_pages;
    state.movies = [...state.movies, ...data.results];
  },
  SET_GENRES(state, data) {
    state.genres = [...state.genres, ...data.genres];
  },
  SELECT_ALL_GENRES(state) {
    state.showAllGenres = true;
    state.selectedGenres = getters.genres(state);
    state.genres.map(genre => (genre.active = true));
    state.genres = [...state.genres];
  },
  DESELECT_ALL_GENRES(state) {
    state.showAllGenres = false;
    state.selectedGenres = [];
    state.genres.map(genre => (genre.active = false));
    state.genres = [...state.genres];
  }
};

const actions = {
  async getMovies({ commit }, page = 1) {
    try {
      commit("START_LOADING");
      const response = await axios.get(
        `${Constants.API_BASE_URL}/movie/now_playing?api_key=${
          Constants.API_KEY
        }&language=${Constants.LANGUAGE}&page=${page}`
      );
      commit("SET_MOVIES", response.data);
      commit("STOP_LOADING");
    } catch (e) {
      console.error(e);
      commit("STOP_LOADING");
    }
  },
  async initData({ dispatch, commit }) {
    await dispatch("getGenres");
    await dispatch("getMovies");
    commit("SELECT_ALL_GENRES");
  },
  async getGenres({ commit }) {
    try {
      const response = await axios.get(
        `${Constants.API_BASE_URL}/genre/movie/list?api_key=${
          Constants.API_KEY
        }&language=${Constants.LANGUAGE}`
      );
      commit("SET_GENRES", response.data);
    } catch (e) {
      console.error(e);
    }
  }
};

const getters = {
  filteredMovies(state) {
    return state.movies
      .filter(movie => {
        if (state.showAllGenres) {
          return true;
        } else {
          return movie.genre_ids.every(id => state.selectedGenres.includes(id));
        }
      })
      .filter(movie => {
        return movie.vote_average >= state.minimumRating;
      });
  },
  genres(state) {
    return [
      ...new Set([].concat(...state.movies.map(movie => movie.genre_ids)))
    ];
  },
  getGenreById: state => id => {
    return state.genres.find(genre => genre.id === id);
  },
  loading(state) {
    return state.loading;
  },
  getMinimumRating(state) {
    return state.minimumRating;
  },
  showAllGenres(state) {
    return state.showAllGenres;
  },
  selectedGenres(state) {
    return state.selectedGenres;
  },
  lastLoadedPage(state) {
    return state.lastLoadedPage;
  },
  totalPages(state) {
    return state.totalPages;
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
